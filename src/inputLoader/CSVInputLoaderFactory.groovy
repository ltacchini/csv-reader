package inputLoader

import lectura.InputLoader
import lectura.InputLoaderFactory

class CSVInputLoaderFactory implements InputLoaderFactory {
	
	CSVInputLoader inputLoader

	@Override
	public InputLoader createInputLoader(URL url) {
		inputLoader = new CSVInputLoader(url)
		return inputLoader
	}

	@Override
	public boolean acceptsURL(URL url) {
		return url.getPath().endsWith(".csv") || url.getPath().endsWith(".txt")
	}
		
}
