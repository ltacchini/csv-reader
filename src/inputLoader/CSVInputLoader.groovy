package inputLoader

import lectura.InputLoader

class CSVInputLoader implements InputLoader {
	
	File file
	def reader 
	int globalId
	int noOfLines

	def lines = new HashMap()
	
	CSVInputLoader() {} // FIXME: esto hay que sacarlo. 
		
	CSVInputLoader(URL url){
		file = new File(url.getPath())
		noOfLines = loadAll()
	}
	
	private loadAll() {
		def line, noOfLines = 0
		file.withReader { reader ->
			while ((line = reader.readLine()) != null) {
				lines.put(noOfLines, line)
				noOfLines++
			}
		}
		return noOfLines
	}

	@Override
	public Object[] next() {
		def line = lines.get(globalId)
		if (line != null) {
			globalId++
			return line.split(",")
		}
		return [] 
	}
	
	@Override
	public boolean hasNext() {
		return globalId != noOfLines
	}
	
}
